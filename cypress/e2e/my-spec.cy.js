//import data from '../fixtures/example.json'
describe('Test Contact App', () => {

  beforeEach(() => {
    cy.visit('./contact_app.html')
    cy.fixture('example').then((example) => {
      cy.wrap(example).as('example');
    })
  })

  it('Test if the application loads correctly', () => {
    cy.get('h1.text-center').should('have.text', 'Contact List App');
    cy.get('table tbody tr').should('have.length', 1)
  })

  // Add tests here
  it('Test if the contact can e added correctly', () => {
    cy.get('@example').then((user) => {
      cy.add(user.name, user.phone, user.email);
      cy.get('tr').should('have.length', 2); 
      cy.get('td').eq(0).should('contain', user.name);
      cy.get('td').eq(1).should('contain', user.phone);
      cy.get('td').eq(2).should('contain', user.email);
    })
  });

  it('Test if the phone and email can be editted correctly', () => {
    cy.get('@example').then((user) => {
      cy.add(user.name, user.phone, user.email);
      cy.edit(1, user.name, user.phone_updated, user.email_updated);
      cy.get('td').eq(0).should('contain', user.name);
      cy.get('td').eq(1).should('contain', user.phone_updated);
      cy.get('td').eq(2).should('contain', user.email_updated);
    })
  });

  it('Test if the name is changed, the phone and email can be deleted correctly', () => {
    cy.get('@example').then((user) => {
      cy.add(user.name, user.phone, user.email);
      cy.edit(1, user.name_updated, user.phone, user.email);
      cy.get('td').eq(0).should('contain', user.name_updated);
      cy.get('td').eq(1).should('contain', '');
      cy.get('td').eq(2).should('contain', '');
    })
  });

  it('Test if the contact can be deleted correctly', () => {
    cy.get('@example').then((user) => {
      cy.add(user.name, user.phone, user.email)
      cy.deleteContact(user.name, user.phone, user.email);
      // Verify the table no longer contains the deleted contact
      cy.get('tr').each(($row) => {
        const rowName = Cypress.$($row).find('td:nth-child(1)').text().trim();
        const rowPhone = Cypress.$($row).find('td:nth-child(2)').text().trim();
        const rowEmail = Cypress.$($row).find('td:nth-child(3)').text().trim();

        expect(rowName).not.to.equal('Jane Doe');
        expect(rowPhone).not.to.equal('1234567890');
        expect(rowEmail).not.to.equal('janedoe@example.com');

      })
  })
  })
});

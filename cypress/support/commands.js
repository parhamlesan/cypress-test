// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('add', (name, phone, email) => {
    cy.get('input[placeholder="Name"]').type(name);
    cy.get('input[placeholder="Phone"]').type(phone);
    cy.get('input[placeholder="Email"]').type(email);
    cy.get('button[name="add"]').click();
});
  
Cypress.Commands.add('edit', (contactIndex, name, phone, email) => {
    cy.get('tr').eq(contactIndex).within(() => {
        cy.get('button[name="edit"]').click();
        cy.get('input[type="text"]').eq(0).clear().type(name);
        cy.get('input[type="text"]').eq(1).clear().type(phone);
        cy.get('input[type="text"]').eq(2).clear().type(email);
        cy.get('button[name="update"]').click();
    });
});
  
Cypress.Commands.add('delete', (contactIndex) => {
    cy.get('tr').eq(contactIndex).within(() => {
        cy.get('button[name="delete"]').click();
    });
});

Cypress.Commands.add('deleteContact', (name, phone, email ) => {
    cy.get('tr:not(:first-child)').each(($row) => {
        const rowName = Cypress.$($row).find('td:nth-child(1)').text().trim();
        const rowPhone = Cypress.$($row).find('td:nth-child(2)').text().trim();
        const rowEmail = Cypress.$($row).find('td:nth-child(3)').text().trim();
        if (rowName == name && rowPhone == phone && rowEmail == email) {
            cy.wrap($row).within(() => {
                cy.get('button[name="delete"]').click();
            });
        }
    })

});
